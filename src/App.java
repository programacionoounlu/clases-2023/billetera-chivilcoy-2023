import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class App {
    private HashMap<Integer, Billetera> billeteras;

    public App(){
        this.billeteras = new HashMap<>();
    }

    public int crearBilletera(String nombre, Dinero saldoInicial) {
        Billetera resultado = new Billetera(saldoInicial, nombre);
        this.billeteras.put(resultado.getCvu(), resultado);
        return resultado.getCvu();
    }

    private Billetera buscarBilletera(int cvu) throws Exception {
        if(this.billeteras.containsKey(cvu)){
            return this.billeteras.get(cvu);
        }else{
            throw new Exception("Billetera inexostente");
        }
    }

    public void transferir(int cvuOrigen, int cvuDestino, Dinero dinero) throws Exception {
        Billetera origen = this.buscarBilletera(cvuOrigen);
        Billetera destino = this.buscarBilletera(cvuDestino);
        origen.extraer(dinero);
        destino.depositar(dinero);
    }

    public Dinero saldo(int cvu) throws Exception {
        return this.buscarBilletera(cvu).saldo();
    }
}
