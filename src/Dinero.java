import java.util.Objects;

public class Dinero {

    private Monedas moneda;
    private Double monto;

    public Dinero(Double monto, Monedas moneda){
        this.monto = monto;
        this.moneda = moneda;
    }

    public Dinero restar(Dinero dinero) {
        return new Dinero(this.monto - dinero.getMonto(), this.moneda);
    }

    public Dinero restar(Double monto){
        return new Dinero(this.monto - monto, this.moneda);
    }

    public Double getMonto() {
        return this.monto;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Dinero dinero = (Dinero) o;
        return moneda == dinero.moneda && Objects.equals(getMonto(), dinero.getMonto());
    }

    @Override
    public int hashCode() {
        return Objects.hash(moneda, getMonto());
    }

    public boolean mayor(Dinero otro) {
        return this.getMonto() > otro.getMonto();
    }

    public Dinero sumar(Dinero dinero) {
        return new Dinero(this.getMonto() + dinero.getMonto(), this.moneda);
    }
}
