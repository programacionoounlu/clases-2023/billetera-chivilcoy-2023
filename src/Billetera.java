public class Billetera {
    private String usuario;
    private Dinero saldo;

    private int cvu;
    private static int proximoCvu = 1;

    public Billetera(Dinero saldo, String usuario){
        this.saldo = saldo;
        this.usuario = usuario;
        this.cvu = Billetera.generarCvu();
    }

    private static int generarCvu() {
        int resultado = Billetera.proximoCvu;
        Billetera.proximoCvu += 1;
        return resultado;
    }

    public void extraer(Dinero dinero) throws Exception {
        if(dinero.mayor(this.saldo)){
            throw new Exception("El saldo de la billetera (" +
                    this.saldo + ") no es suficiente para extraer "
                    + dinero
            );
        }
        this.saldo = this.saldo.restar(dinero);
    }

    public Dinero saldo() {
        return this.saldo;
    }

    public void depositar(Dinero dinero) {
        this.saldo = this.saldo.sumar(dinero);
    }

    public int getCvu() {
        return this.cvu;
    }
}
