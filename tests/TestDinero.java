import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

public class TestDinero {

    @Test
    public void testMayor(){
        Dinero d1 = new Dinero(100.00, Monedas.DOLAR);
        Dinero d2 = new Dinero(101.00, Monedas.DOLAR);
        Assertions.assertFalse(d1.mayor(d2));
        Assertions.assertTrue(d2.mayor(d1));
    }

    @Test
    public void testSuma(){
        Dinero d1 = new Dinero(100.00, Monedas.DOLAR);
        Dinero d2 = new Dinero(101.00, Monedas.DOLAR);
        Assertions.assertEquals(
                new Dinero(201.00, Monedas.DOLAR),
                d1.sumar(d2)
        );
    }
}
