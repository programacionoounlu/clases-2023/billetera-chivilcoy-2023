import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

public class TestBilletera {

    @Test
    public void testExtraccionConFondosSuficientes() throws Exception {
        Dinero saldoInicial = new Dinero(2000.00, Monedas.DOLAR);
        Billetera billetera = new Billetera(saldoInicial, "Juana");
        billetera.extraer(new Dinero(1000.00, Monedas.DOLAR));
        Assertions.assertEquals(
                billetera.saldo(),
                new Dinero(1000.00, Monedas.DOLAR)
        );
    }

    @Test
    public void testExtraccionSinFondosSuficientes(){
        Billetera billetera = new Billetera(new Dinero(0.0, Monedas.DOLAR), "Juana");
        Assertions.assertThrowsExactly(
                // Tipo de excepción esperada
                Exception.class,
                // Código "peligroso"
                () -> {
                        billetera.extraer(new Dinero(100.0, Monedas.DOLAR));
                });
    }

    @Test
    public void testDepositar(){
        Billetera billetera = new Billetera(new Dinero(100.0, Monedas.DOLAR), "Juana");
        billetera.depositar(new Dinero(1500.0, Monedas.DOLAR));
        Assertions.assertEquals(
                new Dinero(1600.0, Monedas.DOLAR),
                billetera.saldo()
        );
    }


}
