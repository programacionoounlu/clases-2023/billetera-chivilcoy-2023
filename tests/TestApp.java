import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

public class TestApp {

    @Test
    public void testTransferencia() throws Exception {
        App aplicacion = new App();
        int cvu1 = aplicacion.crearBilletera("Juan", new Dinero(100.00, Monedas.DOLAR));
        int cvu2 = aplicacion.crearBilletera("José", new Dinero(100.00, Monedas.DOLAR));
        aplicacion.transferir(cvu1, cvu2, new Dinero(100.00, Monedas.DOLAR));
        Assertions.assertEquals(
                new Dinero(0.0, Monedas.DOLAR),
                aplicacion.saldo(cvu1)
        );
        Assertions.assertEquals(
                new Dinero(200.0, Monedas.DOLAR),
                aplicacion.saldo(cvu2)
        );
    }
}
